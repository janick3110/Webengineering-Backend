package com.springboottutorial.controller;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.springboottutorial.model.Building;
import com.springboottutorial.repository.BuildingsRepository;
import com.springboottutorial.service.MainService;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.keycloak.TokenVerifier;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@RestController
@RequestMapping("/api/")
public class Controller {

    String secret = "{" +
            "  \"e\": \"AQAB\"," +
            "  \"kty\": \"RSA\"," +
            "  \"n\": \"qG9JW0yR6S5ZxLxBUUC-RDKUdRIzwgxbY2cdAPLa8XXq5YZmzijEZ5kB5YdoNpWNIaGHjoJlTwY-3p31SGxws9h5w8qInC05i0YHX4b4akAbdA7d7yf117pDkwPg8MPl7RPUyrIiYi922o42gQL1h1Y4KvrDOdv6Fk4L8M0tjYWA7e9AVUE6O1Q1AawwmjLOrbYFdSSCCWs8BXeMMs5DAvk14ogK-F7rUtmzR_kioPUWQwJVZSJj8XdtzyfDnXGLpDvGE1UWmy3g2-7yo4GDFJFyyNTBk76KQ3BDfqpbWyZEQJS7ZaKrfbPlzGIwD9nYZjM1dbthQoZXY0rSY7Go8Q\"" +
            "}";
    JSONObject secretJson = new JSONObject(secret);
    String algorithm = "HmacSHA256";

    @Autowired
    private BuildingsRepository buildingRepository;

    @Autowired
    private MainService mainService;

    @GetMapping("/jwt")
    public String jwt(@RequestHeader MultiValueMap<String, String> headers) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return "/api/jwt test";
    }

    @GetMapping("/ath")
    public ResponseEntity<String> auth(@RequestHeader MultiValueMap<String, String> headers) {
        HttpResponse<String> response;
        try {
            response = Unirest.post("http://localhost/auth/realms/biletado/protocol/openid-connect/token")
                    .header("content-type", "application/x-www-form-urlencoded")
                    .body("grant_type=client_credentials&client_id=backend&client_secret=lMMUNaGY9pM8NYAb0Z8kplk8llAeG0Ba")
                    .asString();

            JSONObject json = new JSONObject(response.getBody());
            String token = json.getString("access_token");

            String publicKeyStr = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqG9JW0yR6S5ZxLxBUUC+RDKUdRIzwgxbY2cdAPLa8XXq5YZmzijEZ5kB5YdoNpWNIaGHjoJlTwY+3p31SGxws9h5w8qInC05i0YHX4b4akAbdA7d7yf117pDkwPg8MPl7RPUyrIiYi922o42gQL1h1Y4KvrDOdv6Fk4L8M0tjYWA7e9AVUE6O1Q1AawwmjLOrbYFdSSCCWs8BXeMMs5DAvk14ogK+F7rUtmzR/kioPUWQwJVZSJj8XdtzyfDnXGLpDvGE1UWmy3g2+7yo4GDFJFyyNTBk76KQ3BDfqpbWyZEQJS7ZaKrfbPlzGIwD9nYZjM1dbthQoZXY0rSY7Go8QIDAQAB";


//            PublicKey publicKey = getKey(publicKeyStr);
//            TokenVerifier<AccessToken> verifier =  TokenVerifier.create(token, AccessToken.class).publicKey(publicKey).verify();
//
//            boolean correct = verifySignature(token, verifier);

            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.setContentType(MediaType.APPLICATION_JSON);
            responseHeaders.setBearerAuth(token);

            return new ResponseEntity<String>(new Building().toString(), responseHeaders, HttpStatus.OK);



        } catch (Exception e) {
            System.out.println("Exception");
        }
        return new ResponseEntity<String>(new Building().toString(), new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/token")
    public String token() {
        try {
            URL url = new URL("http://localhost/auth/realms/biletado/protocol/openid-connect/token");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            con.setDoInput(true);
            con.setDoOutput(true);

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", "rachel"));
            params.add(new BasicNameValuePair("password", "rachel"));
            params.add(new BasicNameValuePair("grant_type", "password"));
            params.add(new BasicNameValuePair("client_id", "backend"));
            params.add(new BasicNameValuePair("client_secret", "lMMUNaGY9pM8NYAb0Z8kplk8llAeG0Ba"));

            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getQuery(params));
            writer.flush();
            writer.close();
            os.close();


            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));

            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    @GetMapping("/")
    public String isRunning() {
        //  Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return "Eigenes Springboot Projekt läuft";
    }

    @GetMapping("/error")
    public String sayError(@RequestParam(value = "myName", defaultValue = "World") String name) {
        return String.format("You did something wrong %s!",name);
    }


    public static PublicKey getKey(String key) {
        try {
            byte[] byteKey = Base64.getDecoder().decode(key.getBytes());
            X509EncodedKeySpec X509publicKey = new X509EncodedKeySpec(byteKey);
            KeyFactory kf = KeyFactory.getInstance("RSA");

            return kf.generatePublic(X509publicKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void hash() {
        try {
            String key = "H";

            SecretKeySpec secret_key = new SecretKeySpec(key.getBytes(), algorithm);
            Mac sha256_HMAC = Mac.getInstance(algorithm);
            sha256_HMAC.init(secret_key);
            byte[] hash = sha256_HMAC.doFinal(key.getBytes());

            StringBuilder result = new StringBuilder();
            for (final byte element : hash)
            {
                result.append(Integer.toString((element & 0xff) + 0x100, 16).substring(1));
            }
        } catch (Exception e) {

        }
    }

    public boolean verifySignature(String token, TokenVerifier<AccessToken> verifier) {
        try {
            String[] chunks = token.split("\\.");

            String message = chunks[0] + "." + chunks[1];
            SecretKeySpec secret_key_real = new SecretKeySpec(secret.getBytes(), algorithm);
            Mac sha256_HMAC_real = Mac.getInstance(algorithm);
            sha256_HMAC_real.init(secret_key_real);
            byte[] tokenHash = sha256_HMAC_real.doFinal(message.getBytes());

            StringBuilder result_real = new StringBuilder();
            for (final byte element : tokenHash)
            {
                result_real.append(Integer.toString((element & 0xff) + 0x100, 16).substring(1));
            }

            byte[] encoded = Base64.getUrlEncoder().encode(result_real.toString().getBytes());

            StringBuilder encodedBuilder = new StringBuilder();
            for (final byte element : encoded)
            {
                encodedBuilder.append(Integer.toString((element & 0xff) + 0x100, 16).substring(1));
            }

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void test() {
//            String[] chunks = token.split("\\.");
//            String head = new String(Base64.getUrlDecoder().decode(chunks[0]));
//            String pay = new String(Base64.getUrlDecoder().decode(chunks[1]));
//            String sig = new String(Base64.getUrlDecoder().decode(chunks[2]));
//            JSONObject header = new JSONObject(head);
//            JSONObject payload = new JSONObject(pay);



//            if (payload.getLong("exp") <= (System.currentTimeMillis() / 1000)) {
//
//            }
//
//
//            String tokenWithoutSignature = chunks[0] + "." + chunks[1];
//
//            String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqG9JW0yR6S5ZxLxBUUC+RDKUdRIzwgxbY2cdAPLa8XXq5YZmzijEZ5kB5YdoNpWNIaGHjoJlTwY+3p31SGxws9h5w8qInC05i0YHX4b4akAbdA7d7yf117pDkwPg8MPl7RPUyrIiYi922o42gQL1h1Y4KvrDOdv6Fk4L8M0tjYWA7e9AVUE6O1Q1AawwmjLOrbYFdSSCCWs8BXeMMs5DAvk14ogK+F7rUtmzR/kioPUWQwJVZSJj8XdtzyfDnXGLpDvGE1UWmy3g2+7yo4GDFJFyyNTBk76KQ3BDfqpbWyZEQJS7ZaKrfbPlzGIwD9nYZjM1dbthQoZXY0rSY7Go8QIDAQAB";
//            String key = "-----BEGIN PUBLIC KEY-----" +
//                    publicKeyStr +
//                    "-----END PUBLIC KEY----";
//            DecodedJWT jwt = JWT.decode(token);
//
//
//            JwkProvider provider = new UrlJwkProvider("http://localhost/auth");
//            Jwk jwk = provider.get(jwt.getKeyId());
//            Algorithm algorithm = Algorithm.RSA256((RSAPublicKey) jwk.getPublicKey(), null);
//            algorithm.verify(jwt);
    }

}
