package com.springboottutorial.controller;

import com.springboottutorial.model.Building;
import com.springboottutorial.model.Error;
import com.springboottutorial.model.Storey;
import com.springboottutorial.repository.BuildingsRepository;
import com.springboottutorial.repository.StoreysRepository;
import com.springboottutorial.service.BuildingService;
import com.springboottutorial.service.MainService;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/assets/buildings/")
public class BuildingsController {

    @Autowired
    private BuildingsRepository buildingsRepository;

    @Autowired
    private StoreysRepository storeysRepository;

    @Autowired
    private MainService mainService;

    @Autowired
    private BuildingService buildingService;

    @Autowired
    private Logger logger;


    @GetMapping("/")
    public ResponseEntity<String> getAllBuildings() {
        LinkedList<Building> buildings = buildingsRepository.findAll();

        logger.info("API call to [{}]: {} entries found",
                "GET /assets/buildings/",
                buildings.size());

        return mainService.getBasicResponseEntity(buildings, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<String> addANewBuilding() {
        if (mainService.isAuthenticated()) {
            String body = mainService.getRequestBody();
            if (buildingService.isCorrectInput(body)) {

                JSONObject jsonBuilding = new JSONObject(body);

                if (buildingService.buildingHasId(jsonBuilding) && buildingService.buildingExists(jsonBuilding)) {
                    Building building = new Building(jsonBuilding);
                    buildingsRepository.save(building);

                    logger.info("API call to [{}]: [200] Updated {}",
                            "POST /assets/buildings/",
                            body);

                    return mainService.getBasicResponseEntity(building, HttpStatus.OK);
                } else {
                    Building building = new Building(jsonBuilding);
                    buildingsRepository.save(building);

                    logger.info("API call to [{}]: [201] Created {}",
                            "POST /assets/buildings/",
                            body);

                    return mainService.getBasicResponseEntity(building, HttpStatus.CREATED);
                }
            } else {
                logger.info("API call to [{}]: [400] Invalid input with {}",
                        "POST /assets/buildings/",
                        body);

                Error error = new Error("Invalid input");
                return mainService.getErrorResponseEntity(error, HttpStatus.BAD_REQUEST);
            }
        } else {
            logger.info("API call to [{}]: [401] No valid authentication",
                    "POST /assets/buildings/");

            Error error = new Error("No valid authentication");
            return mainService.getErrorResponseEntity(error, HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/{id}/")
    public ResponseEntity<String> getABuildingById(@PathVariable UUID id) {
        Optional<Building> building = buildingsRepository.findById(id);

        if (building.isPresent()) {
            logger.info("API call to [{}]: [204] Found {}",
                    "GET /assets/buildings/{id}/",
                    new JSONObject(building.get()));

            return mainService.getBasicResponseEntity(building.get(), HttpStatus.OK);
        } else {
            logger.info("API call to [{}]: [404] Could not find building with id {}",
                    "GET /assets/buildings/{id}/",
                    id);

            Error error = new Error("Building not found");
            return mainService.getErrorResponseEntity(error, HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}/")
    public ResponseEntity<String> addOrUpdateABuildingById(@PathVariable UUID id) {
        if (mainService.isAuthenticated()) {
            String body = mainService.getRequestBody();

            if (buildingService.isCorrectInput(body)) {
                JSONObject jsonBuilding = new JSONObject(body);
                String idString = jsonBuilding.getString("id");
                UUID jsonId = UUID.fromString(idString);

                if (jsonId.equals(id)) {
                    Building building = new Building(jsonBuilding);
                    buildingsRepository.save(building);

                    logger.info("API call to [{}]: [204] Saved {}",
                            "PUT /assets/buildings/{id}/",
                            body);
                    return mainService.getEmptyResponseEntity(HttpStatus.NO_CONTENT);
                } else {
                    logger.info("API call to [{}]: [422] Mismatching id in url ({}) and object ({})",
                            "PUT /assets/buildings/{id}/",
                            id,
                            jsonId);

                    Error error = new Error("Mismatching id in url and object");
                    return mainService.getErrorResponseEntity(error, HttpStatus.UNPROCESSABLE_ENTITY);
                }
            } else {
                logger.info("API call to [{}]: [400] Invalid input with {}",
                        "PUT /assets/buildings/{id}/",
                        body);

                Error error = new Error("Invalid input");
                return mainService.getErrorResponseEntity(error, HttpStatus.BAD_REQUEST);
            }
        } else {
            logger.info("API call to [{}]: [401] No valid authentication",
                    "PUT /assets/buildings/{id}/");

            Error error = new Error("No valid authentication");
            return mainService.getErrorResponseEntity(error, HttpStatus.UNAUTHORIZED);
        }
    }

    @DeleteMapping("/{id}/")
    public ResponseEntity<String> deleteABuildingById(@PathVariable UUID id) {
        if (mainService.isAuthenticated()) {
            Optional<Building> building = buildingsRepository.findById(id);

            if (building.isPresent()) {
                //check for existing storeys
                Iterable<Storey> iter = storeysRepository.findAll();
                for (Storey storey : iter) {
                    if (storey.getBuildingId().equals(id)) {
                        logger.info("API call to [{}]: [422] Building with id {} could not be deleted: cause is " +
                                        "existing storey",
                                "DELETE /assets/buildings/{id}/",
                                id);
                        Error error = new Error("Deletion not possible because of existing storey");
                        return mainService.getErrorResponseEntity(error, HttpStatus.UNPROCESSABLE_ENTITY);
                    }
                }

                buildingsRepository.delete(building.get());

                logger.info("API call to [{}]: [204] Deleted building with id {}",
                        "DELETE /assets/buildings/{id}/",
                        id);

                return mainService.getEmptyResponseEntity(HttpStatus.NO_CONTENT);
            } else {
                logger.info("API call to [{}]: [404] Building with id {} not found",
                        "DELETE /assets/buildings/{id}/",
                        id);

                Error error = new Error("Building could not be found");
                return mainService.getErrorResponseEntity(error, HttpStatus.NOT_FOUND);
            }
        } else {
            logger.info("API call to [{}]: [401] No valid authentication",
                    "DELETE /assets/buildings/{id}/");

            Error error = new Error("No valid authentication");
            return mainService.getErrorResponseEntity(error, HttpStatus.UNAUTHORIZED);
        }
    }
}
