package com.springboottutorial.service;

import com.springboottutorial.model.Building;
import com.springboottutorial.repository.BuildingsRepository;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.InvalidObjectException;
import java.util.Optional;
import java.util.UUID;

@org.springframework.stereotype.Service
public class BuildingService {

    @Autowired
    private BuildingsRepository buildingsRepository;

    public boolean isCorrectInput(String input) {
        try {
            JSONObject jsonBuilding = new JSONObject(input);

            String name = jsonBuilding.getString("name");
            String address = jsonBuilding.getString("address");

            if (name.equals("") | address.equals("")) {
                throw new InvalidObjectException("");
            }

            return true;
        } catch (JSONException | InvalidObjectException e) {
            return false;
        }
    }

    public boolean buildingHasId(JSONObject building) {
        try {
            String idString = building.getString("id");
            UUID id = UUID.fromString(idString);
            return true;
        } catch (JSONException |IllegalArgumentException e) {
            return false;
        }
    }

    public boolean buildingExists(JSONObject jsonBuilding) {
        Optional<Building> optional = buildingsRepository.findById(UUID.fromString(jsonBuilding.getString("id")));
        return optional.isPresent();
    }
}
