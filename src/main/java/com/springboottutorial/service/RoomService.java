package com.springboottutorial.service;

import com.springboottutorial.model.Reservation;
import com.springboottutorial.model.Room;
import com.springboottutorial.repository.RoomsRepository;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InvalidObjectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.Optional;
import java.util.UUID;

@Service
public class RoomService {

    @Autowired
    private RoomsRepository roomsRepository;

    @Value("${backend.reservation.url}")
    private String reservationUrl;

    public boolean isCorrectInput(String input) {
        try {
            JSONObject jsonRoom = new JSONObject(input);

            String name = jsonRoom.getString("name");
            String storeyIdString = jsonRoom.getString("storey_id");

            if (name.equals("") | storeyIdString.equals("")) {
                throw new InvalidObjectException("");
            }

            return true;
        } catch (JSONException | InvalidObjectException e) {
            return false;
        }
    }

    public boolean roomHasId(JSONObject room) {
        try {
            String idString = room.getString("id");
            UUID id = UUID.fromString(idString);
            return true;
        } catch (JSONException |IllegalArgumentException e) {
            return false;
        }
    }

    public boolean roomExists(JSONObject jsonRoom) {
        Optional<Room> optional = roomsRepository.findById(UUID.fromString(jsonRoom.getString("id")));
        return optional.isPresent();
    }

    public Iterable<Reservation> getAllReservations() throws IOException {
       URL url = new URL(reservationUrl);
       HttpURLConnection con = (HttpURLConnection) url.openConnection();
       con.setRequestMethod("GET");

       BufferedReader in = new BufferedReader(
               new InputStreamReader(con.getInputStream()));

       String inputLine;
       StringBuilder content = new StringBuilder();
       while ((inputLine = in.readLine()) != null) {
           content.append(inputLine);
       }
       in.close();


       JSONArray jsonArray = new JSONArray(content.toString());

       LinkedList<Reservation> allReservations = new LinkedList<Reservation>();

       for (int i = 0; i < jsonArray.length(); i++) {
           JSONObject jsonObject = jsonArray.getJSONObject(i);
           Reservation reservation = new Reservation(jsonObject);
           allReservations.add(reservation);
       }

       return allReservations;
    }
}
