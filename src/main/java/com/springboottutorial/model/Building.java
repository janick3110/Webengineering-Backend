package com.springboottutorial.model;

import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name="buildings")
public class Building {

    @Id
    UUID id;

    String name;
    String address;

    public Building() {

    }

    public Building(JSONObject json) {
        name = json.getString("name");
        address = json.getString("address");

        try {
            id = UUID.fromString(json.getString("id"));
        } catch (JSONException e) {
            id = UUID.randomUUID();
        }
    }


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
