package com.springboottutorial.model;

import org.json.JSONObject;

public class Error {

    private String message;
    private JSONObject additionalProp1 = new JSONObject();

    public Error(String message) {
        this.message = message;
    }

    public String toString() {
        return "{" + "\r\n" +
                "  \"message\": \"" + message + "\",\r\n" +
                "  \"additionalProp1\": " + additionalProp1 + "\r\n" +
                "}";
//        JSONObject error = new JSONObject();
//        error.put("message", message);
//        error.put("additionalProp1", additionalProp1);
//        return error.toString();
    }

    public String getMessage() {
        return message;
    }

    public JSONObject getAdditionalProp1() {
        return additionalProp1;
    }
}
