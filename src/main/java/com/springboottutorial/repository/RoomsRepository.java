package com.springboottutorial.repository;

import com.springboottutorial.model.Room;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RoomsRepository extends CrudRepository<Room, UUID> {

}
